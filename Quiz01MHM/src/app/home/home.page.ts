import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  nm: number[] = [0,1,2,3,4,5];

  colors: string[] = ["#00FF00","cyan","blue", "red", "yellow", "orange"];

  constructor() {}

  json = {
    "empresa" : "La Aceitera",
    "sistema" : {
        "nombre": "Contabilidad",
        "area" : "administración",
            "usuarios" : [
                {
                    "nombre" : "Alberto",
                    "apellidos" : "Carreón Dominguez",
                    "edad" : 25,
                    "foto" : "https://gpluseurope.com/wp-content/uploads/Mauro-profile-picture.jpg",
                    "color": "green"
                },
                {
                    "nombre" : "Ana",
                    "apellidos" : "Gutierrez Gómez",
                    "edad" : 28,
                    "foto" : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
                    "color": "cyan"
                },
                {
                    "nombre" : "Luis Roberto",
                    "apellidos" : "Rentería Aguilar",
                    "edad" : 35,
                    "foto" : "https://linksys.i.lithium.com/t5/image/serverpage/image-id/17360i094A9CC74F39EE12/image-size/original?v=1.0&px=-1",
                    "color": "blue"
                },
                {
                    "nombre" : "Diana Isela",
                    "apellidos" : "Herrera Valdéz",
                    "edad" : 22,
                    "foto" : "http://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-112.jpg",
                    "color": "red"
                },
                {
                    "nombre" : "Alma Rosa",
                    "apellidos" : "Velazquez López",
                    "edad" : 28,
                    "foto" : "http://lavinephotography.com.au/wp-content/uploads/2017/01/WEBRebecca-Bryant_7296-.jpg",
                    "color": "yellow"
                },
                {
                    "nombre" : "Jorge Humberto",
                    "apellidos" : "Salazar Hernández",
                    "edad" : 62,
                    "foto" : "http://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-107.jpg",
                    "color": "orange"
                }
            ],
        "actualizado" : true
    }
  }

}




